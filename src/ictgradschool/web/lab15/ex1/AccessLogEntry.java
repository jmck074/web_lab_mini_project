package ictgradschool.web.lab15.ex1;

import java.util.Date;

public class AccessLogEntry {
    private int id;
    private String name;
    private String des;
    private Date time_stampt;

    void setId(int a){ this.id = a;}
    void setName(String a){ this.name = a;}
    void setDes(String a){ this.des = a;}
    void setTime_stampt(Date a){ this.time_stampt = a;}

    int getId() {return this.id;};
    String getName() {return this.name;}
    String getDes() {return this.des;}
    Date getTime() {return this.time_stampt;}
}
